/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator.test;

import static org.amdatu.testing.configurator.TestConfigurator.*;

import org.osgi.service.log.LogService;

import junit.framework.TestCase;

public class ComponentDefinitionTest extends TestCase {
	
	public static interface ServiceWithDependency {
		void assertLogServiceInjected();
		String sayHello();
	}
	
	public static class ServiceWithDependencyImpl implements ServiceWithDependency {

		private volatile LogService m_logService;
		
		@Override
		public void assertLogServiceInjected() {
			assertNotNull(m_logService);
		}
		
		@Override
		public String sayHello() {
			return "Hello";
		}
		
	}
	
	private volatile ServiceWithDependency m_service;
	
	public void setUp() throws Exception {
		super.setUp();
		
		configure(this)
			.add(component()
					.setInterface(ServiceWithDependency.class.getName(), null)
					.setImplementation(ServiceWithDependencyImpl.class)
					.add(serviceDependency(LogService.class).setRequired(true)))
			.add(serviceDependency(ServiceWithDependency.class).setRequired(true))
			.apply();
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		cleanUp(this);
	}
	
	public void testComponentCreated() {
		assertEquals("Hello", m_service.sayHello());
		m_service.assertLogServiceInjected();
	}
}
