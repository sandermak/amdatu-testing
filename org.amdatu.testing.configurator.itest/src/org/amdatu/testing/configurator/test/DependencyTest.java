/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.serviceDependency;

import java.lang.reflect.Proxy;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import org.amdatu.testing.configurator.InjectionFailedException;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentStateListener;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

public class DependencyTest extends TestCase {
    private final BundleContext m_context = FrameworkUtil.getBundle(DependencyTest.class).getBundleContext();

    private volatile Provider m_provider;

    /**
     * Test whether the test case can handle service dynamics through the use of the track() configuration step.
     */
    public void testServiceInjectionWithDynamicsOk() throws Exception {
        DependencyManager dm = new DependencyManager(m_context);
        Component c = dm.createComponent()
            .setInterface(Provider.class.getName(), null)
            .setImplementation(ProviderImpl.class);

        TestComponentStateListener listener = new TestComponentStateListener();
        c.addStateListener(listener);

        configure(this)
            .add(serviceDependency(Provider.class).setRequired(false))
            .apply();

        dm.add(c);

        assertTrue(listener.m_startLatch.await(5, TimeUnit.SECONDS));
        assertNotNull(m_provider);
        assertFalse("Did not expect a NullObject", Proxy.isProxyClass(m_provider.getClass()));

        dm.remove(c);

        assertTrue(listener.m_stopLatch.await(5, TimeUnit.SECONDS));
        assertNotNull(m_provider);
        assertTrue("Expected a NullObject", Proxy.isProxyClass(m_provider.getClass()));

        c.removeStateListener(listener);
        listener = new TestComponentStateListener();
        c.addStateListener(listener);

        dm.add(c);

        assertTrue(listener.m_startLatch.await(5, TimeUnit.SECONDS));
        assertNotNull(m_provider);
        assertFalse("Did not expect a NullObject", Proxy.isProxyClass(m_provider.getClass()));

        cleanUp(this);
        dm.remove(c);
    }

    /**
     * Test whether the test case can handle service dynamics through the use of the track() configuration step.
     */
    public void testRequiredServiceInjectionOk() throws Exception {
        DependencyManager dm = new DependencyManager(m_context);
        Component c =
            dm.createComponent().setInterface(Provider.class.getName(), null).setImplementation(ProviderImpl.class);

        try {
            // This should fail, as Provider is not available as service...
            configure(this)
                .setTimeout(50, TimeUnit.MILLISECONDS)
                .add(serviceDependency(Provider.class).setRequired(true))
                .apply();

            fail("InjectionFailedException expected!");
        } catch (InjectionFailedException e) {
            // Ok; expected...
            cleanUp(this);
        }

        dm.add(c);

        try {
            configure(this)
                .setTimeout(50, TimeUnit.MILLISECONDS)
                .add(serviceDependency(Provider.class).setRequired(true))
                .apply();
        } finally {
            dm.remove(c);

            cleanUp(this);
        }
    }

    static class TestComponentStateListener implements ComponentStateListener {
        final CountDownLatch m_startLatch = new CountDownLatch(1);
        final CountDownLatch m_stopLatch = new CountDownLatch(1);

        @Override
        public void started(Component c) {
            m_startLatch.countDown();
        }

        @Override
        public void starting(Component c) {
            // nop
        }

        @Override
        public void stopped(Component c) {
            m_stopLatch.countDown();
        }

        @Override
        public void stopping(Component c) {
            // nop
        }
    }
}
