/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

import static org.amdatu.testing.configurator.TestConfigurator.wrap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentDeclaration;
import org.apache.felix.dm.ComponentDependencyDeclaration;
import org.apache.felix.dm.ComponentStateListener;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.ServiceDependency;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

public class TestConfiguration {
    private final Object m_testCase;
    private final BundleContext m_context;
    private final List<ConfigurationStep> m_steps;
    private final DependencyManager m_dm;

    private Component m_component;
    private long m_timeout;

    TestConfiguration(Object testCase) {
        Objects.requireNonNull(testCase, "TestCase cannot be null!");
        m_testCase = testCase;

        Bundle bundle = FrameworkUtil.getBundle(m_testCase.getClass());
        Objects.requireNonNull(bundle, "Not running as OSGi test case?!");

        m_context = bundle.getBundleContext();
        m_dm = new DependencyManager(m_context);

        m_steps = new ArrayList<>();
        m_timeout = TimeUnit.SECONDS.toMillis(5L);
    }

    /**
     * Adds a given {@link ConfigurationStep} to this container.
     * 
     * @param step the step to add, cannot be <code>null</code>.
     * @return this container, never <code>null</code>.
     */
    public TestConfiguration add(ConfigurationStep step) {
        Objects.requireNonNull(step, "Step cannot be null!");
        m_steps.add(step);
        return this;
    }

    /**
     * Adds a given {@link Component} to this container.
     * 
     * @param component the component to add, cannot be <code>null</code>.
     * @return this container, never <code>null</code>.
     */
    public TestConfiguration add(Component component) {
        Objects.requireNonNull(component, "Component cannot be null!");
        m_steps.add(wrap(component));
        return this;
    }

    /**
     * Adds a given {@link ServiceDependency} to this container.
     * 
     * @param dependency the service dependency to add, cannot be <code>null</code>.
     * @return this container, never <code>null</code>.
     */
    public TestConfiguration add(ServiceDependency dependency) {
        Objects.requireNonNull(dependency, "ServiceDependency cannot be null!");
        m_steps.add(wrap(dependency));
        return this;
    }

    /**
     * Sets the timeout to wait until all dependencies are satisfied.
     * 
     * @param duration the duration of the timeout;
     * @param unit the unit of time.
     * @return this container, never <code>null</code>.
     */
    public TestConfiguration setTimeout(long duration, TimeUnit unit) {
        m_timeout = unit.toMillis(duration);
        return this;
    }

    /**
     * Applies this configuration to the given test case.
     * 
     * @param testCase
     */
    public void apply() {
        m_component = m_dm.createComponent().setImplementation(m_testCase);

        for (ConfigurationStep step : m_steps) {
            step.apply(m_testCase);
        }

        final CountDownLatch latch = new CountDownLatch(1);
        m_component.addStateListener(new ComponentStateListener() {
            @Override
            public void stopping(Component c) {
            }

            @Override
            public void stopped(Component c) {
            }

            @Override
            public void starting(Component c) {
            }

            @Override
            public void started(Component c) {
                latch.countDown();
            }
        });

        m_dm.add(m_component);

        try {
            if (!latch.await(m_timeout, TimeUnit.MILLISECONDS)) {
            	TestConfigurator.cleanUp(m_testCase);
                throw new InjectionFailedException("Failed to configure test case: "
                    + "not all dependencies were satisfied:\n" + listNotAvailComponents());
            }
        } catch (InterruptedException e) {
        	TestConfigurator.cleanUp(m_testCase);
            throw new InjectionFailedException("Failed to configure test case: interrupted by another thread!", e);
        }
    }

    public void cleanUp() {
        try {
            for (ConfigurationStep step : m_steps) {
                step.cleanUp(m_testCase);
            }
        } finally {
            m_dm.remove(m_component);
        }
    }

    final Component getComponent() {
        return m_component;
    }

    final DependencyManager getDependencyManager() {
        return m_dm;
    }

    final Object getTestCase() {
        return m_testCase;
    }

    /*
     * Returns the list of unregistered services and their dependencies and
     * their dependencies' status.
     * String built like 'notavail' output from dm shell.
     * Shamelessly stolen and adapted from org.apache.felix.dm.shell.DMCommand
     */
    @SuppressWarnings("unchecked")
    private String listNotAvailComponents() {
        StringBuilder sb = new StringBuilder();
        final DependencyManagerSorter SORTER = new DependencyManagerSorter();
        List<DependencyManager> managers = DependencyManager.getDependencyManagers();
        Collections.sort(managers, SORTER);
        long lastBundleId = -1;
        for (DependencyManager manager : managers) {
            List<Component> components = manager.getComponents();
            for (Component component : components) {
                ComponentDeclaration sc = (ComponentDeclaration) component;
                String name = sc.getName();
                int state = sc.getState();
                Bundle bundle = sc.getBundleContext().getBundle();
                long bundleId = bundle.getBundleId();
                if (state == ComponentDeclaration.STATE_UNREGISTERED) {
                    if (lastBundleId != bundleId) {
                        lastBundleId = bundleId;
                        sb.append("[" + bundleId + "] " + bundle.getSymbolicName() + "\n");
                    }
                    sb.append("  " + name + " " + ComponentDeclaration.STATE_NAMES[state] + "\n");
                    ComponentDependencyDeclaration[] dependencies = sc.getComponentDependencies();
                    if (dependencies != null && dependencies.length > 0) {
                        for (ComponentDependencyDeclaration dep : dependencies) {
                            String depName = dep.getName();
                            String depType = dep.getType();
                            int depState = dep.getState();
                            sb.append("    " + depName + " " + depType + " "
                                + ComponentDependencyDeclaration.STATE_NAMES[depState] + "\n");
                        }
                    }
                }
            }
        }
        return sb.toString();
    }

    private static class DependencyManagerSorter implements Comparator<DependencyManager> {
        public int compare(DependencyManager dm1, DependencyManager dm2) {
            long id1 = dm1.getBundleContext().getBundle().getBundleId();
            long id2 = dm2.getBundleContext().getBundle().getBundleId();
            return id1 > id2 ? 1 : -1;
        }
    }

}
