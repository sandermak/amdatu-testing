/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationEvent;
import org.osgi.service.cm.ConfigurationListener;
import org.osgi.util.tracker.ServiceTracker;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TestUtils {

    public static <T> T getService(Class<T> serviceClass) {
        try {
            return getService(serviceClass, null);
        } catch (InvalidSyntaxException e) {
            throw new IllegalStateException(serviceClass + " service is not available");
        }
    }
    
    @SuppressWarnings({ "unchecked" })
    private static <T> T getService(Class<T> serviceClass, String filterString) throws InvalidSyntaxException {
        T serviceInstance = null;

        ServiceTracker serviceTracker;
        BundleContext context = getBundleContext();

        if (filterString == null) {
            serviceTracker = new ServiceTracker(context, serviceClass.getName(), null);
        } else {
            filterString = String.format("(&(%s=%s)%s)", Constants.OBJECTCLASS, serviceClass.getName(), filterString);
            serviceTracker = new ServiceTracker(context, context.createFilter(filterString), null);
        }
        
        serviceTracker.open();
        
        try {
            serviceInstance = (T) serviceTracker.waitForService(2 * 1000);

            if (serviceInstance == null) {
                throw new IllegalStateException(serviceClass + " service is not available");
            } 
            return serviceInstance;
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new IllegalStateException(serviceClass + " service is not available");
        } 
    }
    
    public static Configuration getConfiguration(String servicePid) {
        ConfigurationAdmin configurationAdmin = getService(ConfigurationAdmin.class);
        try {
            Configuration configuration = configurationAdmin.getConfiguration(servicePid, null);
            return configuration;
        } catch (IOException ex) {
            throw new RuntimeException("Exception while configuring service for " + servicePid, ex);
        }
    }
    
    public static void deleteConfiguration(final String servicePid) {
        ConfigurationAdmin configurationAdmin = getService(ConfigurationAdmin.class);
        try {
            BundleContext context = getBundleContext();

            final CountDownLatch deleteLatch = new CountDownLatch(1);
            ConfigurationListener listener = new ConfigurationListener() {
                
                @Override
                public void configurationEvent(ConfigurationEvent event) {
                    if(event.getPid().equals(servicePid) && event.getType() == ConfigurationEvent.CM_DELETED) {
                        deleteLatch.countDown();
                    } 
                }
            };
            ServiceRegistration registration = context.registerService(ConfigurationListener.class.getName(), listener, null);
            Configuration configuration = configurationAdmin.getConfiguration(servicePid, null);
            configuration.delete();
            
            boolean success = deleteLatch.await(2, TimeUnit.SECONDS);
            registration.unregister();
            
            if(!success) {
                throw new RuntimeException("Waiting too long for deletion of configuration " + servicePid);
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Exception while deleting configuration " + servicePid, e);
        }
    }
    
    public static Configuration getFactoryConfiguration(String factoryPid) {
        ConfigurationAdmin configurationAdmin = getService(ConfigurationAdmin.class);
        try {
            Configuration configuration = configurationAdmin.createFactoryConfiguration(factoryPid, null);
            return configuration;
        } catch (IOException ex) {
            throw new RuntimeException("Exception while configuring factory", ex);
        }
    }

    private static BundleContext getBundleContext() {
        Bundle bundle = FrameworkUtil.getBundle(TestConfigurator.class);
        Objects.requireNonNull(bundle, "No bundle available? Are we running in a OSGi test container?!");
        return bundle.getBundleContext();
    }

}
