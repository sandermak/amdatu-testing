/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

import static org.amdatu.testing.configurator.TestUtils.deleteConfiguration;
import static org.amdatu.testing.configurator.TestUtils.getService;
import static org.osgi.framework.Constants.OBJECTCLASS;
import static org.osgi.framework.Constants.SERVICE_PID;
import static org.osgi.framework.FrameworkUtil.createFilter;

import java.io.IOException;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Configuration step that configures a single OSGi service factory.
 */
public class ServiceConfigurator implements ConfigurationStep {
    /**
     * Configures a service asynchronously by using a {@link ConfigurationAdmin} service.
     */
    final class AsyncServiceConfigurator implements ConfigurationStep {
        @Override
        public void apply(Object testCase) {
            ConfigurationAdmin configurationAdmin = getService(ConfigurationAdmin.class);
            try {
                Configuration configuration;
                if (m_factory) {
                    configuration = configurationAdmin.createFactoryConfiguration(m_servicePid, null);
                } else {
                    configuration = configurationAdmin.getConfiguration(m_servicePid, null);
                }
                configuration.update(m_properties);

                m_provisionedPid = configuration.getPid();
            } catch (IOException ex) {
                throw asRuntimeException(ex);
            }
        }

        @Override
        public void cleanUp(Object testCase) {
            if (m_provisionedPid != null) {
                deleteConfiguration(m_provisionedPid);
            }
        }
    }

    /**
     * Configures a service synchronously by directly invoking the registered {@link ManagedServiceFactory} or {@link ManagedService}.
     */
    final class SyncServiceConfigurator implements ConfigurationStep {
        @Override
        public void apply(Object testCase) {
            try {
                ServiceTracker tracker = new ServiceTracker(getBundleContext(testCase), createServiceFilter(), null);
                tracker.open();

                Object service = tracker.waitForService(m_timeout);
                if (service == null) {
                    throw new RuntimeException("Unable to find service!");
                }

                if (m_factory) {
                    // Generate a particular PID for this factory...
                    String pid = UUID.randomUUID().toString();

                    ((ManagedServiceFactory) service).updated(pid, m_properties);

                    m_provisionedPid = pid;
                } else {
                    ((ManagedService) service).updated(m_properties);

                    m_provisionedPid = m_servicePid;
                }

                // We're done...
                tracker.close();
            } catch (ConfigurationException | InvalidSyntaxException ex) {
                throw asRuntimeException(ex);
            } catch (InterruptedException ex) {
                // Ok; we're stopping already...
            }
        }

        @Override
        public void cleanUp(Object testCase) {
            try {
                ServiceTracker tracker = new ServiceTracker(getBundleContext(testCase), createServiceFilter(), null);
                tracker.open();

                Object service = tracker.waitForService(m_timeout);
                if (service == null) {
                    throw new RuntimeException("Unable to find service!");
                }

                if (m_factory) {
                    ((ManagedServiceFactory) service).deleted(m_provisionedPid);
                } else {
                    ((ManagedService) service).updated(null);
                }

                // We're done...
                tracker.close();
            } catch (ConfigurationException | InvalidSyntaxException ex) {
                throw asRuntimeException(ex);
            } catch (InterruptedException ex) {
                // Ok; we're stopping already...
            }
        }

        private Filter createServiceFilter() throws InvalidSyntaxException {
            String type = (m_factory ? ManagedServiceFactory.class : ManagedService.class).getName();
            return createFilter(String.format("(&(%s=%s)(%s=%s))", OBJECTCLASS, type, SERVICE_PID, m_servicePid));
        }
    }

    private static final long DEFAULT_TIMEOUT = 5000; /* ms */

    private final Properties m_properties;
    private final String m_servicePid;
    private final boolean m_factory;

    private long m_timeout;
    private boolean m_synchronousDelivery;
    private String m_provisionedPid;
    private ConfigurationStep m_delegate;

    private ServiceConfigurator(String servicePid, boolean factory) {
        m_properties = new Properties();
        m_servicePid = servicePid;
        m_factory = factory;
        m_timeout = DEFAULT_TIMEOUT;
        m_synchronousDelivery = false;
        m_provisionedPid = null;
    }

    public static ServiceConfigurator createFactoryServiceConfigurator(String servicePid) {
        return new ServiceConfigurator(servicePid, true);
    }

    public static ServiceConfigurator createServiceConfigurator(String servicePid) {
        return new ServiceConfigurator(servicePid, false);
    }

    public void apply(Object testCase) {
        if (m_synchronousDelivery) {
            m_delegate = new SyncServiceConfigurator();
        } else {
            m_delegate = new AsyncServiceConfigurator();
        }
        m_delegate.apply(testCase);
    }

    public void cleanUp(Object testCase) {
    	m_delegate.cleanUp(testCase);
    }

    public ServiceConfigurator set(String key, Object value) {
        m_properties.put(key, value);
        return this;
    }

    public ServiceConfigurator setSynchronousDelivery(boolean synchronousDelivery) {
        m_synchronousDelivery = synchronousDelivery;
        return this;
    }

    public ServiceConfigurator setTimeout(long duration, TimeUnit unit) {
        m_timeout = unit.toMillis(duration);
        return this;
    }

    BundleContext getBundleContext(Object testCase) {
        Bundle bundle = FrameworkUtil.getBundle(testCase.getClass());
        return bundle.getBundleContext();
    }
    
    RuntimeException asRuntimeException(Exception ex) {
        return new RuntimeException(String.format("Exception while%sconfiguring for %s!",
            m_factory ? " factory " : " ", m_servicePid), ex);
    }
}
