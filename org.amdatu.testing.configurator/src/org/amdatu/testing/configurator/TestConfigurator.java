/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

import java.util.Dictionary;
import java.util.Objects;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.ServiceDependency;

/**
 * Utilities for helping out with common OSGi integration test operations such as
 * configuring and retrieving service instances.
 * 
 * Usage:
 * 
 * <ul>
 * <li> Make sure your test case inherits from JUnit's <code>TestCase</code> class.</li>
 * <li> Import the public methods defined in this class statically to your test case </li>
 * <li> Define a private volatile field for each service you want to inject to your test </li>
 * <li> Override the setUp() method, and do the necessary configurations. </li>
 * <li> Override the tearDown() method and call the static cleanUpTest() method.
 * This will revert any persistent changes done by any of the configuration steps. </li>
 * </ul>
 * Here is an example of configuring a service called MyService that uses the amdatu mongodb service:
 * 
 * </ul>
 * <pre>
 * <code>
 * import static org.amdatu.testing.configurator.TestConfigurator.*;
 * // other necessary imports
 * 
 * public class MyTestCase extends TestCase {
 * private volatile MyService myService;
 * 
 * public void setUp() {
 * configureTest(this,
 * configureFactory("org.amdatu.mongo").set("dbName", "test"),
 * inject(MyService.class));
 * }
 * 
 * public void tearDown() {
 * cleanUpTest(this);
 * }
 * 
 * public void testSomething() {
 * assertNotNull(myService);
 * }
 * }
 * </code>
 * </pre>
 */
public final class TestConfigurator {
    private static ThreadLocal<TestConfiguration> CONFIGS = new ThreadLocal<>();

    private TestConfigurator() {
        // Avoid instances being made...
    }

    /**
     * Sets up a JUnit test case, performing all specified configurations and registering
     * all necessary services.
     * 
     * @return the container of configuration steps.
     */
    public static TestConfiguration configure(Object testCase) {
        TestConfiguration container = CONFIGS.get();
        if (container != null) {
            throw new RuntimeException("Multiple test configurations not supported! Did you forget to invoke org.amdatu.testing.configurator.TestConfigurator.cleanUp(this) after your test?");
        }
        container = new TestConfiguration(testCase);
        CONFIGS.set(container);
        return container;
    }

    /**
     * @param testCase the test case to clean up, cannot be <code>null</code>.
     */
    public static void cleanUp(Object testCase) {
        TestConfiguration container = CONFIGS.get();
        if (container != null) {
            try {
                container.cleanUp();
            } finally {
                CONFIGS.remove();
            }
        }
    }

    /**
     * Configures the specified service factory.
     * 
     * @param pid The pid of the service factory to configure
     * @return ServiceConfigurator instance used to set configuration properties.
     */
    public static ServiceConfigurator factoryConfiguration(String pid) {
        return ServiceConfigurator.createFactoryServiceConfigurator(pid);
    }

    /**
     * Configures a service with the specified pid.
     * 
     * @param pid The pid of the service to configure.
     * @return ServiceConfigurator instance used to set configuration properties.
     */
    public static ServiceConfigurator configuration(String pid) {
        return ServiceConfigurator.createServiceConfigurator(pid);
    }

    /**
     * Creates a new {@link Component} instance, for adding custom service implementations at runtime.
     * 
     * @return an empty {@link Component} instance, never <code>null</code>.
     */
    public static Component component() {
        TestConfiguration container = getContainer();
        return container.getDependencyManager().createComponent();
    }

    /**
     * Creates a new {@link Component} instance and sets the service implementation and interfaces to the given arguments.
     * <p>
     * This is merely a convenience method for adding a simple service implementation.
     * </p>
     * 
     * @return an empty {@link Component} instance, never <code>null</code>.
     */
    public static Component component(Object serviceImpl, Class<?>... services) {
        return component(serviceImpl, null, services);
    }

    /**
     * Creates a new {@link Component} instance and sets the service implementation, properties and interfaces to the given arguments.
     * <p>
     * This is merely a convenience method for adding a simple service implementation.
     * </p>
     * 
     * @return an empty {@link Component} instance, never <code>null</code>.
     */
    public static Component component(Object serviceImpl, Dictionary<?, ?> serviceProps, Class<?>... services) {
        String[] ifaces = new String[services.length];
        for (int i = 0; i < ifaces.length; i++) {
            ifaces[i] = services[i].getName();
        }
        Component comp = component().setImplementation(serviceImpl);
        if (ifaces.length > 0) {
            comp.setInterface(ifaces, serviceProps);
        }
        return comp;
    }

    /**
     * Injects an empty, unconfigured, service dependency.
     * 
     * @return a {@link ServiceDependency} builder allowing you to customize the injection further.
     */
    public static ServiceDependency serviceDependency() {
        TestConfiguration container = getContainer();
        return container.getDependencyManager().createServiceDependency();
    }

    /**
     * Injects a service dependency for the given service type.
     * 
     * @param serviceClass The class of the service to inject.
     * @return a {@link ServiceDependency} builder allowing you to customize the injection further.
     */
    public static ServiceDependency serviceDependency(Class<?> serviceClass) {
        return serviceDependency(serviceClass, null);
    }

    /**
     * Injects a service dependency that satisfies the specified filter.
     * 
     * @param serviceClass The class of the service to inject.
     * @param filter The service filter
     * @return {@link ServiceDependency} builder allowing you to customize the injection further.
     */
    public static ServiceDependency serviceDependency(Class<?> serviceClass, String filter) {
    	ServiceDependency sdep = serviceDependency();
    	return sdep.setService(serviceClass, filter).setRequired(true);
    }
    
    static TestConfiguration getContainer() {
        TestConfiguration result = CONFIGS.get();
        Objects.requireNonNull(result, "No test-step container found; is it configured properly?!");
        return result;
    }

    static ConfigurationStep wrap(final Object step) {
        Objects.requireNonNull(step, "Step cannot be null!");
        if (step instanceof Component) {
            return wrap((Component) step);
        } else if (step instanceof ServiceDependency) {
            return wrap((ServiceDependency) step);
        } else if (step instanceof ConfigurationStep) {
            return (ConfigurationStep) step;
        } else {
            throw new IllegalArgumentException("Unable to wrap: " + step.getClass().getSimpleName() + "!");
        }
    }

    private static ConfigurationStep wrap(final Component component) {
        return new ConfigurationStep() {
            @Override
            public void apply(Object testCase) {
                getContainer().getDependencyManager().add(component);
            }

            @Override
            public void cleanUp(Object testCase) {
                getContainer().getDependencyManager().remove(component);
            }
        };
    }

    private static ConfigurationStep wrap(final ServiceDependency dependency) {
        return new ConfigurationStep() {
            @Override
            public void apply(Object testCase) {
                getContainer().getComponent().add(dependency);
            }

            @Override
            public void cleanUp(Object testCase) {
                getContainer().getComponent().add(dependency);
            }
        };
    }
}
