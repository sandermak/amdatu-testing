/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

import static org.amdatu.testing.configurator.TestConfigurator.wrap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A container for one or several <code>ConfigurationStep</code>s.
 * The container can be used to define a list of configuration steps 
 * in an application to be later reused in integration tests.
 * The steps can be run (and cleaned up) either on every test case
 * or only once for all tests running in the same JVM by calling <code>runOncePerSuite(identifierString)</code>. 
 * 
 * The following example defines a ConfigurationSteps container for performing database configuration
 * and then uses it in a TestCase.The database configuration will be performed only once per the entire 
 * TestCase run.
 * 
 * <code><pre>
 * public class MyTestFunctions {
 * 	public static ConfigurationSteps databaseConfiguration() {
 * 		return ConfigurationSteps.create()
 * 			.add(factoryConfiguration()...)
 * 			.add(configuration()...)
 *			.runOncePerSuite("myDatabaseConfiguration");
 * 	}
 * }
 * 
 * public class MyDatabaseTest extends TestCase {
 * 	public void setUp() throws Exception {
 * 		super.setUp();
 * 		configure(this)
 * 			.add(databaseConfiguration())
 * 			.add(...) // any other steps required for this test case
 * 			.apply();
 * 
 *  	}
 *  	public void tearDown() throws Exception {
 *  		cleanUp(this);	
 *  	}
 * }
 * </pre></code>
 */
public class ConfigurationSteps implements ConfigurationStep {
	
	private boolean m_runOncePerSuite;
	private String m_identifier;
	private List<ConfigurationStep> m_steps;
	
	private static Set<String> BLACKLIST = new HashSet<>();
			
	private ConfigurationSteps(Object ... steps) {
		m_steps = new ArrayList<>();
		m_runOncePerSuite = false;
		for (Object obj : steps) {
			m_steps.add(wrap(obj));
		}
	}

	/**
	 * Creates a <code>ConfigurationSteps</code> container.
	 * 
	 * @param Optional list of configuration steps.
	 * @return <code>ConfigurationSteps</code> instance.
	 */
	public static ConfigurationSteps create(Object ... steps) {
		ConfigurationSteps instance = new ConfigurationSteps(steps);
		return instance;
	}
	
	/**
	 * This method should be used to specify that the steps in this container
	 * should be applied once in a given 'test suite' and should not be removed on cleanUp.
	 * Effectively this means that the configuration will only be applied once per all test cases 
	 * that run on the same JVM.
	 * <p> 
	 * This option should only be used for complex and time consuming configurations
	 * that are identical for all tests. An example might be setting up a database or other
	 * shared resource. Since the configuration is never cleaned up automatically, make sure
	 * you manually clean up any necessary data after your test (for example deleting records
	 * in the configured database instance).
	 *  
	 * @param identifier A unique identifier for this container.
	 * @return <code>ConfigurationSteps</code> instance for further customization. 
	 */
	public ConfigurationSteps runOncePerSuite(String identifier) {
		m_runOncePerSuite = true;
		m_identifier = identifier;
		return this;
	}
	
	/**
	 * Adds the specified configuration step to the container
	 * @param step The step to add.
	 * @return this <code>ConfigurationSteps</code> instance for further configuration.
	 */
	public ConfigurationSteps add(Object step) {
		m_steps.add(wrap(step));
		return this;
	}

	@Override
	public void apply(Object testCase) {
		if (m_runOncePerSuite && BLACKLIST.add(m_identifier) == false) {
			// already run
			return;
		}
		for (ConfigurationStep step : m_steps) {
			step.apply(testCase);
		}
	}

	@Override
	public void cleanUp(Object testCase) {
		if (m_runOncePerSuite) {
			return;
		}
		for (ConfigurationStep step : m_steps) {
			step.cleanUp(testCase);
		}
	}
}
